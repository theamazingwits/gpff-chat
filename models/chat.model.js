const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    senderId: String,
    receiverId:String,
    senderUserId: String, // Sender User Id for Reference
    message: String,
    type: String, //1-text msg, 2-image, 3-audio, 4-video, 5-pdf or doc
    filename: String,
    filepath:String,
    messagetime:String,
    is_read: Number
}, {
    timestamps: true,
    //strict: false,
    collection:"gpffchat"
});

module.exports = mongoose.model('gpffchat', NoteSchema);
