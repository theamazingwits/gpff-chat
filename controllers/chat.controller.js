const chatDB = require('../models/chat.model');

//Insert the Chat message
exports.insert = (msgObject) => {
    const chatDBObj = new chatDB(msgObject);

    chatDBObj.save()
        .then((data) => {
            //res.status(200).json({status:"success", message:"success", data:data});
        }).catch((err) => {
            /* res.status(500).json({
                status:"failure",
                message: err.message || "Some error occurred while creating the tracking."
            }); */
        })
   
};

//Get the chat History
exports.getChatHistory = (req, res) => {
    console.log(req.body);
    var reqParams = req.body;
    if(!reqParams.senderId) {
        res.json({status:"failure", message:"SenderId is empty"})
    }

    if(!reqParams.receiverId) {
        res.json({status:"failure", message:"ReceiverId is empty"});
    }
    
    var qry = {"$or":[{senderId:reqParams.senderId,receiverId:reqParams.receiverId},
            {senderId:reqParams.receiverId, receiverId:reqParams.senderId}]}
    chatDB.find(qry).sort({"_id":1})
        .then((data) => {
            res.status(200).json({status:"success", data:data});
        }).catch((err) => {
            res.status(500).json({
                message: err.message || "Some error occurred while retrieving tracking."
            });
        })
}


//Get the chat List
exports.getChatList = (req, res) => {
    console.log(req.body);
    var reqParams = req.body;

    if(!reqParams.user_id) {
        res.json({status:"failure", message:"UserID is empty"})
    }
    var qry ={ $or: [ { senderId: { $in: reqParams.user_id } }, { receiverId: { $in: reqParams.user_id } } ] }
    chatDB.find(qry).sort({"createdAt":-1})
        .then((data) => {
            res.status(200).json({data});
        }).catch((err) => {
            res.status(500).json({
                message: err.message || "Some error occurred while retrieving tracking."
            });
        })
}

//Don't delete below lines
/* exports.testAWS = (req, res) => {
    fs.readFile(fileName, (err, data) => {
        if(err) throw err;
        const params={
            Bucket: 'dev-cls',
            Key:'Chat/test1.jpg',
            Body: JSON.stringify(data, null, 2)
        }

        s3.upload(params, function(s3Err, data) {
            if (s3Err) throw s3Err
            console.log(`File uploaded successfully at ${data.Location}`)
        });

    })
    res.json({status:"Sucess"});
} */

// update the message read status

exports.updateMessage = (req, res)=>{
    console.log(req.body);
    let query = {"receiverId": req.body.receiverId, "is_read":0}
    let condition = {"senderId": req.body.senderId, "receiverId": req.body.receiverId};
    let update = {"is_read":1}
    let options = {multi: true}
    chatDB.updateMany(condition, update, options).exec();
    chatDB.find(query).count().exec().then(data=>{
        res.json({status: 'success', message:'', data:data});
    }).catch((err) => {
            res.status(500).json({
                message: err.message || "Some error occurred."
            });
        });
}

exports.getTotalCountChats = (req, res)=>{
    console.log(req.body);
    let query = {"receiverId": req.body.receiverId, "is_read":0}
    chatDB.find(query).count().then(data=>{
        res.json({status: 'success', message:'Total unread Message', data:data});
    }).catch((err) => {
            res.status(500).json({
                message: err.message || "Some error occurred."
            });
        });
}