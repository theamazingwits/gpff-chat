var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");
var config = require("./config");
//DB Configuration
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
const chat = require('./controllers/chat.controller');
var multer = require('multer');
const AWS = require('aws-sdk');
var multerS3 = require('multer-s3');
var ab2str = require('arraybuffer-to-string');
const s3Config = require('./config/s3.config');
var fs = require('fs');
// const s3 = new AWS.S3({
//   accessKeyId: s3Config.key,
//   secretAccessKey: s3Config.secretKey,
//   bucket: s3Config.bucket
// });
AWS.config.update({ accessKeyId: s3Config.key, secretAccessKey: s3Config.secretKey});
const s3 = new AWS.S3();
// var upload = multer({
//   storage: multerS3({
//     s3: s3,
//     bucket: s3Config.bucket,
//     acl: 'public-read',
//     storageClass: 'REDUCED_REDUNDANCY',
//     contentType: multerS3.AUTO_CONTENT_TYPE,
//     metadata: function (req, file, cb) {
//       cb(null, {fieldName: file.fieldname});
//     },
//     key: function (req, file, cb) {
//       //cb(null, "Chat/"+file.fieldname + "_" + Date.now() + "_" + file.originalname)
//       cb(null, "Chat/"+ file.originalname);
//     }
//   })
// }) 
const params = {
  Bucket: s3Config.bucket,
  Key: Date.now()+'_image', // type is not required
  Body: 1,
  ACL: 'public-read-write',
  ContentEncoding: 'base64', // required
  ContentType: `image/jpg` // required. Notice the back ticks
}

mongoose.Promise = global.Promise;

var app = express();

var server = require("http").Server(app);
var io = require("socket.io").listen(server);
var userArray = {}
var loginUser = []
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// module.exports.bucket = (new couchbase.Cluster(config.couchbase.server)).openBucket(config.couchbase.bucket);

app.use(express.static(path.join(__dirname, "public")));
app.use("/scripts", express.static(__dirname + "/node_modules/"));

var routes = require("./routes/routes.js")(app);
var ChatModel = require("./models/chatmodel.js");


// Connecting to the database
console.log(dbConfig.url);
mongoose.connect(dbConfig.url, {
  useNewUrlParser: true
}).then(() => {
  console.log("Successfully connected to the database");    
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});


io.on("connection", function(socket){

   // =========== New user register ====== //
   socket.on("register",function(id,callback){
     if(id in userArray){
       console.log("user exits");
       callback(false)
     } else{
       console.log("user is not exits");
       socket.userId = id;
       userArray[socket.userId] = socket;
       loginUser.push(id)
       io.sockets.emit('newUser')
       console.log("=========User array ====",loginUser)
       callback(true)
     }
   })

  // ========= Message ========= //
  socket.on("chating", function(data){
    
    io.emit('refresh_feed',data);
    io.emit('chating',data);
    io.emit('start_typing',data);
    io.emit('stop_typing',data);
      // The upload() is used instead of putObject() as we'd need the location url and assign that to our user profile/database
      // see: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
      
    if(data.type == '1')
    {
        data.filename = "";
      console.log("---->",data.filename);
    
     // console.log("====chating ====",JSON.stringify(data))
      chat.insert(data);
      //chat.getChatHistory(req, res);
      console.log("1",data);
      if(data.receiverId in userArray){
        userArray[data.receiverId].emit('chating',data)
      }else{
        console.log("===User is not found in userArray")
      }
    }
    else if(data.type == 2)
      {

        file = data.filename;
        console.log('file_name'+file);
        const base64Data = new Buffer(file.replace(/^data:image\/\w+;base64,/, ""), 'base64')
  
        // Getting the file type, ie: jpeg, png or gif
        const type = file.split(';')[0].split('/')[1]
        
        // Generally we'd have a userId associated with the image
        // For this example, we'll simulate one
        const userId = Date.now()+"_image";
        
        // With this setup, each time your user uploads an image, will be overwritten.
        // To prevent this, use a unique Key each time.
        // This won't be needed if they're uploading their avatar, hence the filename, userAvatar.js.
        const params = {
          Bucket: s3Config.bucket,
          Key: `${userId}.${type}`, // type is not required
          Body: base64Data,
          ACL: 'public-read-write',
          ContentEncoding: 'base64', // required
          ContentType: `image/${type}` // required. Notice the back ticks
        }
        
        data.filename = userId+'.'+type;
      console.log("---->",data.filename);
      s3.upload(params, (err, data) => {
        if (err) { return console.log(err+"----"); }
      console.log('Image successfully uploaded.');
      });

     // console.log("====chating ====",JSON.stringify(data))
      chat.insert(data);
      //chat.getChatHistory(req, res);
      console.log("2",data);
      if(data.receiverId in userArray){
       
        userArray[data.receiverId].emit('chating',data)
      }else{
        console.log("===User is not found in userArray")
      }
      }


      
  });

  // ========== Start Typing ========== //
  socket.on('start_typing',function(data){
    console.log("3",data);
    console.log("==== start_typing =====",JSON.stringify(data));
    if(data.receiverId in userArray){
      userArray[data.receiverId].emit('start_typing',data)
    }else{
      console.log("===User is not found in userArray")
    }
  });

  // ========== Stop Typing ========== //
  socket.on('stop_typing',function(data){
    console.log("==== stop_typing =====",JSON.stringify(data));
    if(data.receiverId in userArray){
      userArray[data.receiverId].emit('stop_typing',data)
    }else{
      console.log("===User is not found in userArray")
    }
  });
});
app.get('/getUser',function(req, res){
  console.log("=====login user ===",loginUser)
  res.send(loginUser)
})
server.listen(3001, function () {
    console.log("Listening on port %s...", server.address().port);
});

app.post('/getChatHistory', function(req, res) {
  chat.getChatHistory(req, res);
});


app.post('/getChatList', function(req, res) {
  chat.getChatList(req, res);
});

app.post('/updateMessage', function(req, res) {
  chat.updateMessage(req, res);
});

app.post('/getTotalCountChats', function(req, res) {
  chat.getTotalCountChats(req, res);
});




//  app.post('/testAWS', upload.array('filename', 3), function(req, res) {
//   res.send('Successfully uploaded ' + req.files.length + ' files!')
  
//  // chat.testAWS(req, res);
// }); 

